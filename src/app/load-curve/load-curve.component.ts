import { Component, OnInit, Input } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { get, sum } from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-load-curve',
  templateUrl: './load-curve.component.html',
  styleUrls: ['./load-curve.component.sass']
})
export class LoadCurveComponent implements OnInit {
  @Input() resultLoadCurve: any;

  lineChartData: ChartDataSets[] = [
  ];

  lineChartLabels: Label[] = [];

  lineChartOptions = {
    responsive: true,
    title: {
      display: true,
      text: 'HouseHold Load Curve Chart'
    },
    scales: {
      yAxes: [{
        id: 'A',
        type: 'linear',
        position: 'left',
      }, {
        id: 'B',
        type: 'linear',
        position: 'right',
        ticks: {
          max: 2,
          min: 0
        }
      }]
    }
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'rgb(88, 233, 237)',
    },
    {
      borderColor: 'rgb(237, 217, 83)',
    },
    {
      borderColor: 'rgb(205, 67, 236)',
    },
    {
      borderColor: 'rgb(237, 11, 59)',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';

  constructor() { }

  ngOnInit() {
    let meaurements = get(this.resultLoadCurve, 'measurements', [])
    let value = meaurements.map((item) => item.value)
    let lightingValue = meaurements.map((item) => item.lightingValue)
    let activeOccupancy = meaurements.map((item) => item.activeOccupancy)
    let occupancy = meaurements.map((item) => item.occupancy)
    let sum_value = sum(value)
    let sum_lightingValue = sum(lightingValue)
    let startTime = '00:00:00'
    let label = `Total: ${sum_lightingValue}, Load Curve`
    let labels = meaurements.map((item) => moment(startTime, 'HH:mm:ss').add(item.time, 'minutes').format('HH:mm:ss'))
    this.lineChartLabels = labels
    this.lineChartData = [{
      data: value,
      label: `Load Curve, Total: ${sum_value}`,
      yAxisID: 'A',
      fill: false,

    }, {
      data: lightingValue,
      label: `Lighting Load, Total: ${sum_lightingValue}`,
      yAxisID: 'A',
      fill: false,

    }, {
      data: occupancy,
      label: `Occupancy`,
      yAxisID: 'B',
      fill: false,

    }, {
      data: activeOccupancy,
      label: 'Active Occupancy',
      yAxisID: 'B',
      fill: false,
    },
    ]
  }

}
