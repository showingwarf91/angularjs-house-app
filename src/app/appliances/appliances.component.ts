import { Component, Input, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { ApiService } from "../services/api.service";
import { Data } from "../model/data.model";
import * as moment from 'moment';
import { DataSource } from '@angular/cdk/table';




export interface AppliancesElement {
  type: string;
  powerInputOn: any;
  powerInputOff: any;
  duration: any;
}

@Component({
  selector: 'app-appliances',
  templateUrl: './appliances.component.html',
  styleUrls: ['./appliances.component.sass']
})
export class AppliancesComponent implements OnInit {
  displayedColumns: string[] = ['type', 'powerInputOn', 'powerInputOff', 'duration'];

  @Input() appliances: any;

  dataSource: AppliancesElement[];

  constructor(private router: Router, private apiService: ApiService) { }

  ngOnInit() {
    this.dataSource = this.appliances.map((item) => {
      return {
        type: item.type,
        powerInputOn: item.operationalModes[0].powerInputOn.toFixed(1),
        powerInputOff: item.operationalModes[0].powerInputOff.toFixed(1),
        duration: item.operationalModes[0].duration,
      }
    })
  }
}
