import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';
import { DataSource } from '@angular/cdk/table';

export interface ActivitiesElement {
  type: string;
  startTime: string;
  endTime: string;
}

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.sass']
})


export class ActivitiesComponent implements OnInit {
  @Input() activities: any;
  displayedColumns: string[] = ['name', 'startTime', 'endTime'];
  dataSource: ActivitiesElement[];

  getFormat(date) {
    return moment(date).format('LL')
  }

  getTime(datetime) {
    return moment(datetime).format('LT')
  }

  constructor() { }

  ngOnInit() {
    this.dataSource = this.activities.map((item) => {
      return {
        name: item.name,
        startTime: this.getTime(item.start.$date),
        endTime: this.getTime(item.end.$date),
      }
    })
  }

}
