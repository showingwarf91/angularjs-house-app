import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs/internal/Observable";
import { Injectable } from "@angular/core";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // let token = window.localStorage.getItem('token');
    // if (token) {
    //   request = request.clone({
    //     setHeaders: {
    //       'secret-key': '$2b$10$WWaHZDLY8EEHJpKpvVfTIeumzw9QcF.1RtEGz9we/jkNAuHfvwdRe'
    //     }
    //   });
    // }
    request = request.clone({
      setHeaders: {
        'secret-key': '$2b$10$WWaHZDLY8EEHJpKpvVfTIeumzw9QcF.1RtEGz9we/jkNAuHfvwdRe'
      }
    });
    return next.handle(request);
  }
}