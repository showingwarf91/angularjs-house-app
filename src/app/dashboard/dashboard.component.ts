import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { ApiService } from "../services/api.service";
import { Data } from "../model/data.model";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {
  panelOpenState = false;

  data: any;

  constructor(private router: Router, private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getData().subscribe(data => {
      this.data = data
    });

  }

}
