# AngularjsHouseApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.18.


## JSON files is hosted
`https://api.jsonbin.io/b/5dc93883c1f85104e53c22b8`
`Secret KEY is '$2b$10$WWaHZDLY8EEHJpKpvVfTIeumzw9QcF.1RtEGz9we/jkNAuHfvwdRe'`
## Development server

Run `npm run dev` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


## CI

CI is integrated with bitbucket pipelines, any pushes to master will lead to a new build deployed to heroku server

#Charts

ng-chart is used to display two axis Y Line chart